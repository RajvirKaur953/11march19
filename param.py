# positional arguments
# def sum(n1,n2):
#     return n1+n2

# a=sum(10,20)
# print(a)

def sum(n1,n2):
    return n1-n2

a=sum(n2=10,n1=20)              #keyword arguments
print(a)

a=sub(10,n1=20)         #it creates a erroe because by default 1 value goes to first keyword
a=sub(10,n2=20)

